/*
 * game.h
 *
 * Created: 2018-10-24 17:51:57
 *  Author: Peszi
 */ 

#ifndef GAME_H_
#define GAME_H_

// IO
#define BUZZER_PIN 1
#define DIODE_PIN 3

#define RIGHT_BTN_PIN 1
#define LEFT_BTN_PIN 1
#define MENU_BTN_PIN 1

#define DIODE_ON PORTD |= (1 << DIODE_PIN)
#define DIODE_OFF PORTD &= ~(1 << DIODE_PIN)
#define DIODE_TOGGLE PORTD ^= (1 << DIODE_PIN)

#define BUZZER_ON PORTB |= (1 << BUZZER_PIN)
#define BUZZER_OFF PORTB &= ~(1 << BUZZER_PIN)

// times indices
#define BOMB_TIME 0
#define PLANT_TIME 1
#define DEFUSE_TIME 2

// time steps
#define BOMB_TIME_STEP 15
#define DEFAULT_TIME_STEP 2

// init time values
#define DEFAULT_BOMB_TIME 180
#define DEFAULT_PLANT_TIME 4
#define DEFAULT_DEFUSE_TIME 20

void GAME_init(void);
void GAME_update(void);
void GAME_updateTime(void);

#endif /* GAME_H_ */