/*
 * bomb-app.c
 *
 * Created: 2018-10-22 17:43:51
 * Author : Peszi
 */ 

#define F_CPU 16000000L

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "game.h"
#include "display.h"

unsigned char timeDiv = 0;

int main(void) {
	
	// setup IO
	DDRD |= (1 << DIODE_PIN);
	DDRB |= (1 << BUZZER_PIN);
	// setup pull-ups
	
	// initialize 1/20s timer CTC
	TCCR1B |= (1 << WGM12)|(1 << CS12);	// ctc enabled, prescaler 256
	OCR1A = 3124; // F_CPU / ((OCR1A + 1) * prescaler) = 1/20hz
	TIMSK1 |= (1 << OCIE1A); // enable timer1a interruption on OCR1A value
	
	// initialize fast timer CTC
	TCCR2A |= (1 << WGM21);
	TCCR2B |= (1 << CS22)|(1 << CS21)|(1 << CS20);
	OCR2A = 77;
	TIMSK2 |= (1 << OCIE2A);  
	
	sei();
	
	DISPLAY_init();
	GAME_init();
	
    while (1) {
		
    }
}

ISR (TIMER2_COMPA_vect) {
	DISPLAY_update();
}

ISR (TIMER1_COMPA_vect) {
	timeDiv++;
	if (timeDiv % 2 == 0) {
		GAME_update();
		DISPLAY_updateAnimation();
	}
	if (timeDiv >= 20) { 
		timeDiv = 0;
		GAME_updateTime();
	}
}
