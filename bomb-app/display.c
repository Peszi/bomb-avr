/*
 * display.c
 *
 * Created: 2018-10-25 17:39:53
 *  Author: Peszi
 */ 

#include <avr/io.h>
#include "display.h"

const unsigned char CHARACTERS_TABLE[][2] = {
	{0xe3, 0xe5}, {0xef, 0xfd}, {0xc7, 0xe7}, {0xc7, 0xf5}, {0xcb, 0xfd}, // 0-4 digits
	{0xd3, 0xf5}, {0xd3, 0xe5}, {0xe7, 0xfd}, {0xc3, 0xe5}, {0xc3, 0xfd}, // 5-9 digits
	{0xf7, 0xff}, {0xef, 0xff}, {0xff, 0xfd}, // animation segments 0-2
	{0xff, 0xf7}, {0xff, 0xef}, {0xfb, 0xff}, // animation segments 3-5
};

enum displayMode currentMode = NORMAL;
unsigned char currentSegment = 0;
unsigned char currentFrame = 0;
unsigned char displayValues[3];

void DISPLAY_init(void) {
	DDRD |= PORTD_SEGMENTS;
	DDRB |= PORTB_DIGITS;
	DDRC |= PORTC_DIGITS;
}

void DISPLAY_update(void) {
	PORTD &= PORTD_SEGMENTS_MASK;
	PORTD |= (1 << currentSegment);
	displaySegment();
	currentSegment++;
	if (currentSegment >= 3) { currentSegment = 0; }
}

void DISPLAY_updateAnimation(void) {
	if (currentMode == ANIMATION) {
		currentFrame++;
		if (currentFrame >= CHARACTERS_ANIMATIONS_COUNT) { currentFrame = 0; }
	}
}

void DISPLAY_showAnimation() {
	currentFrame = 0;
	currentMode = ANIMATION;
}

void DISPLAY_showNumber(unsigned int number) {
	unsigned char value = number % 100;
	displayValues[2] = number / 100;
	displayValues[1] = value / 10;
	displayValues[0] = value % 10;
	currentMode = NORMAL;
}

void DISPLAY_showTime(unsigned int seconds) {
	unsigned char secs = seconds % 60;
	displayValues[2] = seconds / 60;
	displayValues[1] = secs / 10;
	displayValues[0] = secs % 10;
	currentMode = NORMAL;
}

// internal display use

void displaySegment(void) {
	PORTB |= PORTB_DIGITS;
	PORTC |= PORTC_DIGITS;
	unsigned char value = 0;
	if (currentMode == NORMAL) {
		value = displayValues[currentSegment];
		if (value >= CHARACTERS_NUMBERS_COUNT) return;
		if (currentSegment == 2) { SHOW_DOT; }
	} else {
		value = currentFrame + CHARACTERS_NUMBERS_COUNT;
	}
	PORTB &= CHARACTERS_TABLE[value][0];
	PORTC &= CHARACTERS_TABLE[value][1];
}

