/*
 * display.h
 *
 * Created: 2018-10-25 17:39:27
 *  Author: Peszi
 */ 

#ifndef DISPLAY_H_
#define DISPLAY_H_

#define DOT_PIN 2

#define PORTD_SEGMENTS 0x07
#define PORTD_SEGMENTS_MASK 0xF8
#define PORTB_DIGITS 0x3C
#define PORTC_DIGITS 0x1E

#define SHOW_DOT PORTC &= ~(1 << DOT_PIN)

#define CHARACTERS_NUMBERS_COUNT 10
#define CHARACTERS_ANIMATIONS_COUNT 6

#define DISPLAY_NORMAL_MODE 0
#define DISPLAY_ANIM_MODE 1

enum displayMode { NORMAL, ANIMATION };

void DISPLAY_init(void);
void DISPLAY_update(void);
void DISPLAY_updateAnimation(void);

void DISPLAY_showAnimation();
void DISPLAY_showNumber(unsigned int number);
void DISPLAY_showTime(unsigned int seconds);

void displaySegment(void);

#endif /* DISPLAY_H_ */