/*
 * game.c
 *
 * Created: 2018-10-24 17:52:23
 *  Author: Peszi
 */ 

#include <avr/io.h>
#include "game.h"
#include "display.h"

const unsigned int TIME_VALUES[3] = {
	DEFAULT_BOMB_TIME, 
	DEFAULT_PLANT_TIME, 
	DEFAULT_DEFUSE_TIME
};

unsigned int digit = 0;

void GAME_init(void) {
	DISPLAY_showAnimation();
}

void GAME_update(void) {
	DIODE_TOGGLE;

	
}

void GAME_updateTime(void) {
	DISPLAY_showTime(digit);
	digit++; if (digit >= 1000) { digit = 0; }
}