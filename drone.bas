$regfile = "m88pdef.dat"
$crystal = 16000000

Const Work_time_step = 2
Const Bomb_time_step = 15
Const Default_plant_time = 22
Const Default_defuse_time = 20
Const Default_bomb_time = 180
Const Timer_sec_value = 65536 - 15625                       ' 1s
Const Timer_short_value = 255 - 196                           ' 1s

Config Timer1 = Timer , Prescale = 256
Timer1 = Timer_sec_value

Config Timer0 = Timer , Prescale = 256
Timer0 = Timer_short_value

Buzzer Alias Portb.1
Diode Alias Portd.3
L_btn Alias Pind.5
R_btn Alias Pind.6
M_btn Alias Pind.7

Declare Sub Showchar()

Dim A_seg As Byte , B_seg As Byte , C_seg As Byte
Dim Digits(3) As Byte , Dots As Byte
Dim Left_btn As Bit , Right_btn As Bit , Menu_btn As Bit

Dim Segidx As Byte , Segval As Byte , Timediv As Byte , Buzzerdiv As Integer , Buzzertick As Byte , Ticks As Single

Dim Displaytime As Byte , Defusetime As Byte , Bombtime As Byte , Mins As Byte , Secs As Byte

Dim Targettimes(3) As Byte , Timeidx As Byte
Dim Game As Bit , Await As Bit , Anim As Byte , working As Bit

Dim A As Bit , Tmp As Byte , Tmp2 As Byte

Ddrc = 30
Ddrb = 60
Ddrd = &H0F

Portc = 0
Portb = 0
Portd.5 = 1
Portd.6 = 1
Portd.7 = 1
Portb.1 = 1

Buzzer = 0
Dots = 1
Diode = 1
Timeidx = 1
Targettimes(1) = Default_bomb_time
Targettimes(2) = Default_plant_time
Targettimes(3) = Default_defuse_time
Game = 0

On Timer1 Oneveryquarter
Enable Timer1
On Timer0 Onsegment
Enable Timer0
Enable Interrupts

Do
   If Game = 0 Then

      If L_btn = 0 Then
      Waitms 10
        If L_btn = 0 And Left_btn = 0 Then
           Left_btn = 1
           If Timeidx = 1 Then
              Tmp = Bomb_time_step
            Else
              Tmp = Work_time_step
            End If
           If Targettimes(timeidx) >= Tmp Then
               Targettimes(timeidx) = Targettimes(timeidx) - Tmp
           End If
        End If
        Else
           Left_btn = 0
      End If

      If R_btn = 0 Then
         Waitms 10
         If R_btn = 0 And Right_btn = 0 Then
            Right_btn = 1
            If Timeidx = 1 Then
              Tmp = Bomb_time_step
            Else
              Tmp = Work_time_step
            End If
            Tmp2 = 255 - Tmp
            If Targettimes(timeidx) <= Tmp2 Then
               Targettimes(timeidx) = Targettimes(timeidx) + Tmp
            End If
         End If
      Else
         Right_btn = 0
      End If

      If M_btn = 0 Then
         Waitms 10
         If M_btn = 0 And Menu_btn = 0 Then
            Menu_btn = 1
            Incr Timeidx
            If Timeidx > 3 Then
               Timeidx = 1
            End If
            Select Case Timeidx
               Case 1 : Dots = 1
               Case 2 : Dots = 2
               Case 3 : Dots = 4
            End Select
            If Timeidx = 1 Then
              Tmp = Bomb_time_step
            Else
              Tmp = Work_time_step
           End If
         End If
      Else
         Menu_btn = 0
      End If

      If R_btn = 0 And M_btn = 0 Then
         Diode = 0
         Wait 1
         Diode = 1
         If R_btn = 0 And M_btn = 0 Then
            Wait 1
            Dots = 0
            Await = 1
            Game = 1
         End If
      Else
         Diode = 1
      End If
      Displaytime = Targettimes(timeidx)
      Mins = Displaytime / 60
      Secs = Displaytime Mod 60
      Digits(1) = Mins
      Digits(2) = Secs / 10
      Digits(3) = Secs Mod 10
   Else
      If R_btn = 0 And L_btn = 0 Then
         Working = 1
      Else
         Working = 0
      End If

      If R_btn = 0 And M_btn = 0 Then
         Diode = 0
         Wait 1
         Diode = 1
         If R_btn = 0 And M_btn = 0 Then
            Wait 1
            ' TODO init dots etc
            Dots = 1
            Game = 0
         End If
      Else
      End If
   End If
Loop

Oneveryquarter:
   Disable Timer1
   Timer1 = Timer_sec_value

   If Game = 0 Then

   Else
      If Await = 1 Then
         If Working = 1 Then                                ' planting
            Shift Dots , Left , 1
            If Dots > 4 Then
               Dots = 1
            Elseif Dots = 0 Then
               Dots = 4
            End If
            Mins = Displaytime / 60
            Secs = Displaytime Mod 60
            Digits(1) = Mins
            Digits(2) = Secs / 10
            Digits(3) = Secs Mod 10
            If Displaytime = 0 Then
               Bombtime = Targettimes(1)
               Displaytime = Bombtime
               Diode = 0
               Await = 0
            End If
            Toggle Buzzer
         Else                                               ' awaiting animation
            Buzzer = 0
            Dots = 0
            Displaytime = Targettimes(2)
            Incr Anim
            If Anim > 5 Then
               Anim = 0
            End If
            Tmp = Anim + 10
            Digits(1) = Tmp
            Digits(2) = Tmp
            Digits(3) = Tmp
         End If
      Else
         If Working = 1 Then
            Shift Dots , Right , 1
            If Dots > 4 Then
               Dots = 1
            Elseif Dots = 0 Then
               Dots = 4
            End If
            If Defusetime = 0 Then

            End If
         Else
            Dots = 1
            If Bombtime = 0 Then

            End If
         End If
         Mins = Displaytime / 60
         Secs = Displaytime Mod 60
         Digits(1) = Mins
         Digits(2) = Secs / 10
         Digits(3) = Secs Mod 10
      End If
   End If


   Incr Timediv
   If Timediv > 3 Then
      Timediv = 0
      If Game = 0 Then
         Buzzer = 0
      Else
         If Await = 1 Then
              If Working = 1 Then
                  Decr Displaytime
              End If
         Else
            If Working = 1 Then
               Decr Defusetime
               Displaytime = Defusetime
            Else
               Displaytime = Bombtime
               Defusetime = Targettimes(3)
               Toggle Diode
            End If
            If Bombtime = 0 Then
              Incr Buzzertick
              If Buzzertick > 10 Then
                 Buzzer = 0
              Else
                  Buzzer = 1    
              End If
            Else
               Decr Bombtime
              Buzzer = 0
              If Bombtime < 10 Then
                 Buzzerdiv = 2
              Elseif Bombtime < 30 Then
                 Buzzerdiv = 5
              Elseif Bombtime < 60 Then
                 Buzzerdiv = 10
              Elseif Bombtime < 120 Then
                 Buzzerdiv = 15
              Elseif Bombtime >= 120 Then
                 Buzzerdiv = 30
              Else
                 Buzzerdiv = 0
              End If
              Incr Buzzertick
              If Buzzertick > Buzzerdiv Then
                 Buzzertick = 0
                 Buzzer = 1
              End If
            End If
         End If
      End If
   End If

   Enable Timer1
Return

Onsegment:
   Disable Timer0
   Timer0 = Timer_short_value

   Incr Segidx
   If Segidx > 3 Then
      Segidx = 1
   End If
   Select Case Segidx
      Case 1:
         Portd.0 = 0
         Portd.1 = 0
         Portd.2 = 1
      Case 2:
         Portd.0 = 0
         Portd.1 = 1
         Portd.2 = 0
      Case 3:
         Portd.0 = 1
         Portd.1 = 0
         Portd.2 = 0
   End Select

   Portb = PORTB or &H3C
   Portc = Portc Or &H1E

   Select Case Digits(segidx)
   Case 0:
      PORTB = PORTB AND &HE3
      PORTC = PORTC AND &HE5
   case 1:
      PORTB = PORTB AND &HEF
      PORTC = PORTC AND &HFD
   case 2:
      PORTB = PORTB AND &HC7
      PORTC = PORTC AND &HE7
   case 3:
      PORTB = PORTB AND &HC7
      PORTC = PORTC AND &HF5
   case 4:
      PORTB = PORTB AND &HCB
      PORTC = PORTC AND &HFD
   case 5:
      Portb = Portb And &HD3
      PORTC = PORTC AND &HF5
   case 6:
      PORTB = PORTB AND &HD3
      PORTC = PORTC AND &HE5
   case 7:
      PORTB = PORTB AND &HE7
      PORTC = PORTC AND &HFD
   case 8:
      Portb = Portb And &HC3
      PORTC = PORTC AND &HE5
   case 9:
      PORTB = PORTB AND &HC3
      PORTC = PORTC AND &HFD
   Case 10:
      PORTB = PORTB AND &HF7
      PORTC = PORTC AND &HFF
   Case 11:
      PORTB = PORTB AND &HEF
      PORTC = PORTC AND &HFF
   Case 12:
      PORTB = PORTB AND &HFF
      PORTC = PORTC AND &HFD
   Case 13:
      PORTB = PORTB AND &HFF
      PORTC = PORTC AND &HF7
   Case 14:
      PORTB = PORTB AND &HFF
      PORTC = PORTC AND &HEF
   Case 15:
      PORTB = PORTB AND &HFB
      Portc = Portc And &HFF
   Case Else:
      Portb = Portb And &HFF
      Portc = Portc And &HFB
   End Select

   Shift Dots , Left , 1
   If Dots.segidx = 1 Then
      Portc.2 = 0
   End If
   Shift Dots , Right , 1

   Enable Timer0
Return

End